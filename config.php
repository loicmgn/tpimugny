<?php
// Chemin relatif vers le fichier de configuration SQL de l'application, afin de savoir sur quelle compte celle-ci doit se connecter
$pathToApplicationSqlConfig = "../configSqlApp.php";
// Chemin relatif vers la page d'accueil de l'application à installer
$pathToApplicationIndex = "../index.php";
// Chemin relatif vers le script SQL de création de la base
$pathToSqlScript = "../blog_m152Contraint.sql";
// Liste des tables qui seront crées par l'application pour gérer les conflits (séparée par des virgules)
$tablesToCreate = "post,media";