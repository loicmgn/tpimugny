<?php
require_once 'dataBase.php';

/**
 * Récupère toute les bases de données du serveur actuellement connecté
 *
 * @return string[]
 */
function getAllDataBases() {
    try {
        $req = dataBase::prepare("SHOW DATABASES");
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return FALSE;
    }
    if ($result != NULL) {
        return $result;
    } else {
        return FALSE;
    }
}

/**
 * Récupère toutes les tables présentes dans la base de donnée spécifiée en paramètres
 *
 * @param string $dataBase
 * @return string[] ResultFromSelect
 */
function getDataBase($dataBase) {
    try {
        $req = dataBase::prepare("SELECT table_name FROM information_schema.tables WHERE table_schema = :dataBase;");
        $req->bindParam(':dataBase', $dataBase, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_NUM);
    } catch (Exception $ex) {
        return FALSE;
    }
    if ($result != NULL) {
        return $result;
    } else {
        return FALSE;
    }
}
