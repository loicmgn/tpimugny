<?php
require_once '../Models/dataBase.php';
/**
 * Lance le script SQL passé en paramètre grâce à l'activation des requêtes multiples avec : "database::setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);"
 *
 * @param string $script
 * @return string ErrorMessage
 */
function launchScript($script)
{
    database::setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
    
    try {
        $req = database::prepare($script);
        $req->execute();
    }
    catch (PDOException $e)
    {
        echo $e->getMessage();
        die();
    }
}