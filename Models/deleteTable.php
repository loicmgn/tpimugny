<?php

require_once '../Models/dataBase.php';

/**
 * Supprime la table passée en paramètre dans la base de données spécifiée 
 *
 * @param string $dbName
 * @param string $tableName
 * @return boolean
 */
function deleteTable($dbName, $tableName) {
    try {
        $req = dataBase::prepare("USE " . $dbName . ";");
        $req->execute();
        $req = dataBase::prepare("DROP TABLE " . $tableName);
        $req->execute();
        return TRUE;
    } catch (Exception $ex) {
        return $ex;
    }
}