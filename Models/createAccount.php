<?php
require_once '../Models/dataBase.php';

/**
 * Crée un compte sur le serveur SQL à l'aides du nom de compte et du mot de passe passé en paramètres et lui donne les droits nécessaire sur la base de donnée séléctionnée auparavant
 *
 * @param string $username
 * @param string $password
 * @param string $database
 * @return boolean
 */
function createAccount($username, $password, $database) {
    try {
        $req = dataBase::prepare("GRANT SELECT, INSERT, DELETE, UPDATE ON :database.* TO :username @'localhost' IDENTIFIED BY :password");
        $req->bindParam(':username', $username, PDO::PARAM_STR);
        $req->bindParam(':password', $password, PDO::PARAM_STR);
        $req->bindParam(':database', $database, PDO::PARAM_STR);
        $req->execute();
        return TRUE;
    } catch (Exception $ex) {
        return FALSE;
    }
}
?>