<?php
/**
 * Récupère toute les contraintes de clés étrangères et les retournes dans un tableau associatif
 *
 * @param string $table
 * @return string[]
 */
function getFKConstraints($table) {
    try {
        $req = dataBase::prepare("SELECT `CONSTRAINT_TYPE` FROM information_schema.table_constraints WHERE `TABLE_NAME` = :table AND `CONSTRAINT_TYPE`= 'FOREIGN KEY'");
        $req->bindParam(':table', $table, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return $ex;
    }
    if ($result != NULL) {
        return $result;
    } else {
        return FALSE;
    }
}