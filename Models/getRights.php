<?php
require_once '../Models/dataBase.php';

/**
 * Récupère les droits au niveau du serveur pour un utilisateur spécifié en paramètres
 *
 * @param string $user
 * @return string[]
 */
function getServerRights($user) {
    try {
        $req = dataBase::prepare("SELECT `PRIVILEGE_TYPE` FROM information_schema.user_privileges WHERE `GRANTEE` = :user");
        $req->bindParam(':user', $user, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return $ex;
    }
    if ($result != NULL) {
        return $result;
    } else {
        return FALSE;
    }
}
/**
 * Récupère les droits au niveau d'une base à l'aide des paramètres spécifiant la base sur laquelle chercher les droits et l'utilisateur en question
 *
 * @param string $user
 * @param string $db
 * @return string[]
 */
function getDbRights($user) {
    try {
        $req = dataBase::prepare("SELECT `PRIVILEGE_TYPE` FROM information_schema.schema_privileges WHERE `GRANTEE` = :user");
        $req->bindParam(':user', $user, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return $ex;
    }
    if ($result != NULL) {
        return $result;
    } else {
        return FALSE;
    }
}
/**
 * Récupère les droits au niveau d'une table spécifique sur une Db spécifique pour un utilisateur, à l'aide des variables passées en paramètres
 *
 * @param string $user
 * @param string $db
 * @param string $table
 * @return string[]
 */
function getTableRights($user, $db, $table) {
    try {
        $req = dataBase::prepare("SELECT `PRIVILEGE_TYPE` FROM `USER_PRIVILEGES` WHERE `GRANTEE` = :user AND `TABLE_SCHEMA` = :db AND `TABLE_NAME` = :table");
        $req->bindParam(':user', $user, PDO::PARAM_STR);
        $req->bindParam(':db', $db, PDO::PARAM_STR);
        $req->bindParam(':table', $table, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return FALSE;
    }
    if ($result != NULL) {
        return $result;
    } else {
        return FALSE;
    }
}
?>