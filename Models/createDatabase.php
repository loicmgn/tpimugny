<?php

require_once '../Models/dataBase.php';

/**
 * Créer une base de donnée en spécifiant le nom de la base à créer à l'aide d'une variable passée en paramètres
 *
 * @param string $databaseName
 * @return boolean
 */
function createDataBase($databaseName) {
    try {
        $req = dataBase::prepare("CREATE DATABASE $databaseName");
        $req->execute();
        return TRUE;
    } catch (Exception $ex) {
        return FALSE;
    }
}