<?php
require_once 'dataBase.php';

function getAllDataBases() {
    try {
        $req = dataBase::prepare("SHOW DATABASES");
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        return FALSE;
    }
    if ($result != NULL) {
        return $result;
    } else {
        return FALSE;
    }
}

function getDataBase($dataBase) {
    try {
        $req = dataBase::prepare("SELECT table_name FROM information_schema.tables WHERE table_schema = :dataBase;");
        $req->bindParam(':dataBase', $dataBase, PDO::PARAM_STR);
        $req->execute();
        $result = $req->fetchAll($fetch_style = PDO::FETCH_NUM);
    } catch (Exception $ex) {
        return FALSE;
    }
    if ($result != NULL) {
        return $result;
    } else {
        return FALSE;
    }
}
