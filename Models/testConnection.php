<?php

/**
 * Teste la connexion à la base de donnée pour un compte spécifique passé en paramètre
 *
 * @param string $host
 * @param string $user
 * @param string $password
 * @return boolean
 */
function testConnection($host, $user, $password)
{

    try {
        $dsn = 'mysql:host=' . $host . ';port=3306';
        $conn = new PDO($dsn, $user, $password, array('charset' => 'utf8'));
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return true;
    } catch (PDOException $e) {
        return false;
    }
}
