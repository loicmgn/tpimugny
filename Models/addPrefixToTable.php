<?php

require_once '../Models/dataBase.php';
/**
 * Modifie le nom de la table passée en paramètres en rajoutant le préfixe avant le nom
 *
 * @param [string] $tableName
 * @param [string] $tableWithPrefix
 * @return boolean
 */
function addPrefixToTable($tableName, $tableWithPrefix) {
    try {
        $req = dataBase::prepare("ALTER TABLE :tableName RENAME TO :tableWithPrefix ");
        $req->bindParam(':tableName', $tableName, PDO::PARAM_STR);
        $req->bindParam(':tableWithPrefix', $tableWithPrefix, PDO::PARAM_STR);
        $req->execute();
        return TRUE;
    } catch (Exception $ex) {
        return FALSE;
    }
}