<?php
require_once "../Models/getDataBase.php";

$selectedDbTables = getDatabase($_SESSION["nameDb"]);
$nameDbSelected = $_SESSION["nameDb"];
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Supprimer des tables</title>

  <!-- Bootstrap core CSS -->
  <link href="../Bootstrap/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles -->
  <link href="../CSS/style.css" rel="stylesheet">

</head>

<body>

  <!-- Page Content -->
  <div id="divContent">
  <section class="headerSection">
    <div class="container">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="mt-4">
                <h1 class="title">Supprimer des tables</h1>
                <h5 class="underTitle">Choisissez les tables à supprimer dans la base de données existante.</h5>
            </div>
        </div>
        <div class="col-lg-3"></div>
      </div>
    </div>
  </section>
  <section style ="height : 60px !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-1"></div>
        <div class ="col-lg-10">
            <div class="content">
                <p class="sectionTitle">Base de données séléctionnée : <?php echo $nameDbSelected ?></p>
            </div>
        </div>
        <div class="col-lg-1"></div>
      </div>
    </div>
  </section>
  <section class = "sectionDeleteTables">
    <div class="container">
    <div class="contentDeleteTables">
      <div class="row">
        <div class='col-lg-1'></div>
        <div class='col-lg-10 mx-auto'>
                <form id="formDeleteTables" name="formDeleteTables">
                  <div class='form-group'>
                    <div class='row'>
                      <div class='col-lg-1'></div>
                        <div class='col-lg-10 mx-auto'>
                        <div class="divCheckboxesDelete">
                          <div class ="row">
                                    <?php
                                      for ($i = 0; $i < (Count($selectedDbTables)); $i++) 
                                      {
                                    ?>
                                            <?php
                                              require_once"../Models/getConstraints.php";
                                              $constraints = getFKConstraints($selectedDbTables[$i][0]);
                                              if ($constraints[0] != null )
                                              {
                                            ?>   
                                                <div class ="col-lg-6">                                                                                  
                                                <input type ="checkbox" disabled name ="checkboxTable[]" value="<?=$selectedDbTables[$i][0];?>">
                                                <label for="checkboxTable" style='border : none !important'><?=$selectedDbTables[$i][0];?></label>
                                            <?php
                                                echo "⚠️";
                                            ?>
                                                </div>
                                            <?php
                                              }
                                              else
                                              {
                                            ?>
                                                <div class ="col-lg-6">
                                                <input type ="checkbox" name ="checkboxTable[]" value="<?=$selectedDbTables[$i][0];?>">
                                                <label for="checkboxTable[]" style='border : none !important'><?=$selectedDbTables[$i][0];?></label>
                                                </div>
                                            <?php
                                              }
                                            ?>
                                          
                                    <?php
                                      }
                                    ?>
                                    </div>
                              </div>
                        </div>
                        <div class='col-lg-1'></div>

                    </div>
                  </div>
                </form>
                </div>
                <div class='col-lg-1'></div>
        </div>
    </div>
    </div>
  </section>
</div>
<section class="btnSection">
    <div class="container">
    <p>⚠️ Les tables marquées par ce symbole sont des tables qui possèdent des contraintes d'intégrité. Veuillez veiller à ne pas les supprimer afin de ne pas créer de conflits.</p>
        <div class = "fixed-bottom">
            <div class="row">
                <div class ="col-lg-2"></div>
                <div class ="col-lg-2">
                  <a href="../index.php" class="btn btn-info btn-lg" role="button">Annuler</a>
                </div>
                <div class ="col-lg-4"></div>
                <div class ="col-lg-1">
                        <a href="../Controllers/deleteTablesController.php" class="btn btn-info btn-lg" role="button">Précédent</a>
                    </div>
                <div class ="col-lg-1">
                    <button name="submitDeleteTables" type="submit" class="btn btn-info btn-lg" form="formDeleteTables" formaction="../Controllers/deleteTablesController.php" formmethod="POST">Supprimer</button>
                </div>
                <div class ="col-lg-2"></div>
            </div>
        </div>
    </div>
    </section>

  <!-- Bootstrap core JavaScript -->
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/jquery/jquery.min.js"></script>
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>
