<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Connexion au serveur SQL</title>

</head>

<body>

  <!-- Page Content -->
  <div id="divContent">
  <section class="headerSection">
    <div class="container">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="mt-4">
                <h1 class="title">Connexion au serveur SQL</h1>
                <h5 class="underTitle">Choisissez le serveur SQL sur lequel vous souhaitez stocker la base de données de l'application</h5>
            </div>
        </div>
        <div class="col-lg-3"></div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
        <div class="content">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class ="col-lg-6">
          <?php
          if  (isset($_SESSION["flag"]))
          {
          ?>
            <div class="alert alert-danger" role="alert">
              Informations de connexion incorrectes !
            </div>
          <?php  
          unset($_SESSION["flag"]);
          }
          ?>
                <form id="formConnectServer" name="formConnectServer">
                        <div class="form-group">
                          <label for="adressServer">Adresse IP / Nom du serveur</label>
                          <input type="text" class="form-control" id="adressServer" name="adressServer">
                        </div>
                        <div class="form-group">
                                <label for="userNameServer">Nom d'utilisateur</label>
                                <input type="text" class="form-control" id="usernameServer" name="usernameServer">
                              </div>
                        <div class="form-group">
                          <label for="passwordServer">Mot de passe</label>
                          <input type="password" class="form-control" id="passwordServer" name="passwordServer">
                        </div>
                      </form>
        </div>
        <div class="col-lg-4"></div>
    </div>
    </div>
  </section>
</div>
  <section class="btnSection">
    <div class="container">
        <div class = "fixed-bottom">
            <div class="row">
                <div class ="col-lg-2"></div>
                <div class ="col-lg-2">
                    <a href="../index.php" class="btn btn-info btn-lg" role="button">Annuler</a>
                </div>
                <div class ="col-lg-4"></div>
                <div class ="col-lg-1">
                        <a href="../index.php" class="btn btn-info btn-lg" role="button">Précédent</a>
                    </div>
                <div class ="col-lg-1">
                    <button type="submit" class="btn btn-info btn-lg" form="formConnectServer" formaction="../Controllers/connectDbServerController.php" formmethod="POST">Suivant</button>
                </div>
                <div class ="col-lg-2"></div>
            </div>
        </div>
    </div>
    </section>

  <!-- Bootstrap core JavaScript -->
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/jquery/jquery.min.js"></script>
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>

</body>
  <!-- Bootstrap core CSS -->
  <link href="../Bootstrap/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles -->
  <link href="../CSS/style.css" rel="stylesheet">
</html>
