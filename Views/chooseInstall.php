<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Choix de l'installation</title>

</head>

<body>

  <!-- Page Content -->
  <div id="divContent">
  <section class="headerSection">
    <div class="container">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="mt-4">
                <h1 class="title">Choix installation</h1>
                <h5 class="underTitle">Choisissez si vous souhaitez créer une nouvelle installation ou utiliser la configuration de l'installation existante.</h5>
            </div>
        </div>
        <div class="col-lg-3"></div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
        <div class="content">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class ="col-lg-6">
                <form id="formChooseInstall" name="formChooseInstall">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-1">
                                    <input name="radiosInstall" checked value="radioNewInstall" id="radioNewInstall" class="form-control" type="radio">
                                </div>
                                <div class="col-7">
                                    <label class="form-control" style="border : none !important" for="radioNewInstall">Créer une nouvelle installation</label>
                                </div>
                                <div class="col-lg-2"></div>
                            </div>                                         
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-1">
                                    <input name="radiosInstall" value="radioExistInstall" id="radioExistInstall" class="form-control" type="radio">
                                </div>
                                <div class="col-7">
                                    <label class="form-control" style="border : none !important" for="radioExistInstall">Utiliser l'installation existante</label>
                                </div>
                                <div class="col-lg-2"></div>
                            </div>
                         </div>
                      </form>
        </div>
        <div class="col-lg-3"></div>
    </div>
    </div>
  </section>
</div>
  <section class="btnSection">
    <div class="container">
        <div class = "fixed-bottom">
            <div class="row">
                <div class ="col-lg-2"></div>
                <div class ="col-lg-2">
                    <a href="../index.php" class="btn btn-info btn-lg" role="button">Annuler</a>
                </div>
                <div class ="col-lg-5"></div>
                <div class ="col-lg-1">
                    <button name ="submitChooseInstall" type="submit" class="btn btn-info btn-lg" form="formChooseInstall" formaction="../Controllers/chooseInstallController.php" formmethod="POST">Suivant</button>
                </div>
                <div class ="col-lg-2"></div>
            </div>
        </div>
    </div>
    </section>

  <!-- Bootstrap core JavaScript -->
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/jquery/jquery.min.js"></script>
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>

</body>

  <!-- Bootstrap core CSS -->
  <link href="../Bootstrap/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles -->
  <link href="../CSS/style.css" rel="stylesheet">

</html>
