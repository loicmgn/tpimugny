<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Ajout d'un préfixe</title>

  <!-- Bootstrap core CSS -->
  <link href="../Bootstrap/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles -->
  <link href="../CSS/style.css" rel="stylesheet">

</head>

<body>

  <!-- Page Content -->
  <div id="divContent">
  <section class="headerSection">
    <div class="container">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="mt-4">
                <h1 class="title">Ajouter un préfixe aux tables</h1>
                <h5 class="underTitle">Choisissez si vous souhaitez ajouter un préfixe aux tables créées ou les laisser telles quelles.</h5>
            </div>
        </div>
        <div class="col-lg-3"></div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
        <div class="content">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class ="col-lg-6">
          <?php
        if  (isset($_SESSION["flag"]))
          {
          ?>
            <div class="alert alert-danger" role="alert">
              Veuillez rentrer un préfixe, ou séléctionner l'option pour ne pas en utiliser !
            </div>
          <?php  
          unset($_SESSION["flag"]);
          }
          ?>
                <form id="formChooseDb" name="formChooseDb">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-1">
                                    <input name="radiosPrefix" checked value="radioPrefix" id="radioPrefix" class="form-control" type="radio">
                                </div>
                                <div class="col-6">
                                    <label class="form-control" style="border : none !important" for="radioPrefix">Préfixe :</label>
                                </div>
                                <div class="col-lg5">                               
                                    <input name="textPrefix" id="textPrefix" class="form-control" type="text" onfocus="document.getElementById('radioPrefix').checked = true;">
                                </div>
                            </div>                                         
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-1">
                                    <input name="radiosPrefix" value="radioNoPrefix" id="radioNoPrefix" class="form-control" type="radio">
                                </div>
                                <div class="col-6">
                                    <label class="form-control" style="border : none !important" for="radioNoPrefix">Ne pas ajouter de préfixe</label>
                                </div>
                                <div class="col-lg5">  
                                </div>
                            </div>
                         </div>
                      </form>
        </div>
        <div class="col-lg-4"></div>
    </div>
    </div>
  </section>
</div>
  <section class="btnSection">
    <div class="container">
        <div class = "fixed-bottom">
            <div class="row">
                <div class ="col-lg-2"></div>
                <div class ="col-lg-2">
                    <a href="../index.php" class="btn btn-info btn-lg" role="button">Annuler</a>
                </div>
                <div class ="col-lg-4"></div>
                <div class ="col-lg-1">
                        <a href="../Controllers/addPrefixController.php" class="btn btn-info btn-lg" role="button">Précédent</a>
                    </div>
                <div class ="col-lg-1">
                    <button type="submit" class="btn btn-info btn-lg" form="formChooseDb" formaction="../Controllers/addPrefixController.php" formmethod="POST">Suivant</button>
                </div>
                <div class ="col-lg-2"></div>
            </div>
        </div>
    </div>
    </section>

  <!-- Bootstrap core JavaScript -->
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/jquery/jquery.min.js"></script>
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>
