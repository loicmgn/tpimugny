<?php 
if(session_status() == 1)
{
    session_start();
}
  require_once "../Models/getDataBase.php";
  $avalaibleDataBases = getAllDataBases();
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Connexion au serveur SQL</title>

  <!-- Bootstrap core CSS -->
  <link href="../Bootstrap/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>

  <!-- Page Content -->
  <div id="divContent">
  <section class="headerSection">
    <div class="container">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="mt-4">
                <h1 class="title">Choix de la base de données</h1>
                <h5 class="underTitle">Choisissez si vous souhaitez créer une nouvelle base de données ou en utiliser une existante</h5>
            </div>
        </div>
        <div class="col-lg-3"></div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
        <div class="content">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class ="col-lg-6">
        <?php
        if (isset($_SESSION["flag"]))
        {
          if  ($_SESSION["flag"] == true)
          {
          ?>
            <div class="alert alert-danger" role="alert">
              Veuillez rentrer un nom pour la base de données à créer !
            </div>
          <?php  
          $_SESSION["flag"] = false;
          }
        }?>
                <form id="formChooseDb" name="formChooseDb">
                  <?php
                    foreach ($_SESSION["rightsOnServer"] as $right )
                    {
                        if  ($right['PRIVILEGE_TYPE'] == "CREATE")
                        {

                  ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-1">
                                    <input name="radioChooseDb" checked value="newDb" id="radioNewDb" class="form-control" type="radio">
                                </div>
                                <div class="col-6">
                                    <label class="form-control" style="border : none !important" for="textNewDb">Nouvelle base :</label>
                                </div>
                                <div class="col-lg5">                               
                                    <input name="textNewDb" id="textNewDb" class="form-control" type="text" onfocus="document.getElementById('radioNewDb').checked = true;">
                                </div>
                            </div>                                         
                        </div>
                  <?php 
                        }
                    } 
                  ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-1">
                                    <input name="radioChooseDb" value="existingDb" id="radioExistingDb" class="form-control" type="radio">
                                </div>
                                <div class="col-6">
                                    <label class="form-control" style="border : none !important" for="radioExistingDb">Utiliser une base existante :</label>
                                </div>
                                <div class="col-lg5">  
                                  <select name="selectExistingDb" id="selectExistingDb" class="form-control" onfocus="document.getElementById('radioExistingDb').checked = true;">                             
                                  <?php                                 
                                    for ($i = 0; $i < count($avalaibleDataBases); $i++ )
                                    {
                                        echo '<option>';
                                        echo implode($avalaibleDataBases[$i]);
                                        echo '</option>';
                                    }
                                  ?>
                                  </select> 
                                </div>
                            </div>
                         </div>
                      </form>
        </div>
        <div class="col-lg-4"></div>
    </div>
    </div>
  </section>
</div>
  <section class="btnSection">
    <div class="container">
        <div class = "fixed-bottom">
            <div class="row">
                <div class ="col-lg-2"></div>
                <div class ="col-lg-2">
                  <a href="../index.php" class="btn btn-info btn-lg" role="button">Annuler</a>
                </div>
                <div class ="col-lg-4"></div>
                <div class ="col-lg-1">
                        <a href="../Controllers/chooseDbController.php" class="btn btn-info btn-lg" role="button">Précédent</a>
                    </div>
                <div class ="col-lg-1">
                    <button name ="submitChooseDb" type="submit" class="btn btn-info btn-lg" form="formChooseDb" formaction="../Controllers/chooseDbController.php" formmethod="POST">Suivant</button>
                </div>
                <div class ="col-lg-2"></div>
            </div>
        </div>
    </div>
    </section>

  <!-- Bootstrap core JavaScript -->
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/jquery/jquery.min.js"></script>
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>

</body>

  <!-- Custom styles -->
  <link href="../CSS/style.css" rel="stylesheet">

</html>
