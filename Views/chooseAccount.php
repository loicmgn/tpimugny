<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Choix du compte SQL</title>

  <!-- Bootstrap core CSS -->
  <link href="../Bootstrap/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles -->
  <link href="../CSS/style.css" rel="stylesheet">

</head>

<body>

  <!-- Page Content -->
  <div id="divContent">
  <section class="headerSection">
    <div class="container">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="mt-4">
                <h1 class="title">Choix du compte SQL</h1>
                <h5 class="underTitle">Choisissez le compte SQL qui sera utilisé par l'application lors de son fonctionnement</h5>
            </div>
        </div>
        <div class="col-lg-3"></div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
        <div class="content">
      <div class="row">
        <div class="col-lg-2"></div>
        <div class ="col-lg-8">
            <?php
if (isset($_SESSION["flag1"])) {
    ?>
            <div class="alert alert-danger" role="alert">
              Les informations de connexion au compte existant sont incorrectes !
            </div>
          <?php
unset($_SESSION["flag1"]);
}
if (isset($_SESSION["flag2"])) {
    ?>
            <div class="alert alert-danger" role="alert">
              Veuillez remplir les champs afin de créer un compte !
            </div>
          <?php
unset($_SESSION["flag2"]);
}
?>
                <form id="formChooseAccount" name="formChooseAccount">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-1">
                                    <input class="form-control-sm" checked name="radioChooseAccount" value="sameAccount" id="radioNewDb" type="radio">
                                </div>
                                <div class="col-lg-11">
                                    <label class="form-control" style="border : none !important" for="radioNewDb">Utiliser le même compte que lors de la configuration</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-1">
                                    <input name="radioChooseAccount" value="otherAccount" id="radioOtherAccount" class="form-control-sm" type="radio">
                                </div>
                                <div class="col-4">
                                    <label class="form-control" style="border : none !important" for="radioOtherAccount">Utiliser un autre compte :</label>
                                </div>
                                <div class="col-lg-7">
                                    <div class = "row">
                                        <div class="col-lg-5">
                                            <label class="form-control" style="border : none !important" for="usernameExistingAccount">Nom de compte :</label>
                                        </div>
                                        <div class="col-lg-7">
                                            <input name="usernameExistingAccount" id="usernameExistingAccount" class="form-control" type="text" onfocus="document.getElementById('radioOtherAccount').checked = true;">
                                        </div>
                                    </div>
                                    <div class = "row">
                                            <div class="col-lg-5">
                                                <label class="form-control" style="border : none !important" for="passwordExistingAccount">Mot de passe :</label>
                                            </div>
                                            <div class="col-lg-7">
                                                <input name="passwordExistingAccount" id="passwordExistingAccount" class="form-control" type="password" onfocus="document.getElementById('radioOtherAccount').checked = true;">
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <?php
foreach ($_SESSION["rightsOnServer"] as $right) {
    if ($right['PRIVILEGE_TYPE'] == "CREATE USER") {

        ?>
                        <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1">
                                        <input name="radioChooseAccount" value="newAccount" id="radioNewAccount" class="form-control-sm" type="radio">
                                    </div>
                                    <div class="col-4">
                                        <label class="form-control" style="border : none !important" for="radioNewAccount">Créer un nouveau compte :</label>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class = "row">
                                            <div class="col-lg-5">
                                                <label class="form-control" style="border : none !important" for="usernameNewAccount">Nom de compte :</label>
                                            </div>
                                            <div class="col-lg-7">
                                                <input name="usernameNewAccount" id="usernameNewAccount" class="form-control" type="text" onfocus="document.getElementById('radioNewAccount').checked = true;">
                                            </div>
                                        </div>
                                        <div class = "row">
                                                <div class="col-lg-5">
                                                    <label class="form-control" style="border : none !important" for="passwordNewAccount">Mot de passe :</label>
                                                </div>
                                                <div class="col-lg-7">
                                                    <input name="passwordNewAccount" id="passwordNewAccount" class="form-control" type="password" onfocus="document.getElementById('radioNewAccount').checked = true;">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <?php
}
}
?>
                    </form>
        </div>
        <div class="col-lg-2"></div>
    </div>
    </div>
  </section>
</div>
  <section class="btnSection">
    <div class="container">
        <div class = "fixed-bottom">
            <div class="row">
                <div class ="col-lg-2"></div>
                <div class ="col-lg-2">
                    <a href="../index.php" class="btn btn-info btn-lg" role="button">Annuler</a>
                </div>
                <div class ="col-lg-4"></div>
                <div class ="col-lg-1">
                        <a href="../Controllers/chooseAccountController.php" class="btn btn-info btn-lg" role="button">Précédent</a>
                    </div>
                <div class ="col-lg-1">
                    <button type="submit" name="btnSubmitAccount" class="btn btn-info btn-lg" form="formChooseAccount" formaction="../Controllers/chooseAccountController.php" formmethod="POST">Suivant</button>
                </div>
                <div class ="col-lg-2"></div>
            </div>
        </div>
    </div>
    </section>

  <!-- Bootstrap core JavaScript -->
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/jquery/jquery.min.js"></script>
  <script src="../Bootstrap/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>
