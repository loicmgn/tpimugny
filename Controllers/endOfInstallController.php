<?php 
define('ORIGINAL_PREFIX', '$$$');
define('LENGTH_REPLACE_DB', 6);
define('LENGTH_REPLACE_TABLE', 4);
if(session_status() == 1)
{
    session_start();
}

require_once "../config.php";
require_once "../Models/createDataBase.php";

$nameDb = $_SESSION["nameDb"];

$arrayTablesToCreate[] = explode(",", $tablesToCreate);
$strVars = sprintf("<?php \n \$hostConfig = \"%s\";\n \$usernameConfig = \"%s\";\n \$passwordConfig = \"%s\";\n \$databaseConfig = \"%s\";", $_SESSION["adressServer"], $_SESSION['usernameDb'], $_SESSION['passwordDb'], $nameDb);
file_put_contents("../" . $pathToApplicationSqlConfig, $strVars);

if ($_SESSION["newDb"] == TRUE)
{
    createDataBase($nameDb);
}
else
{
    if (isset ($_SESSION["tablesToDelete"]) && $_SESSION["tablesToDelete"][0] != "")
    {
        require_once "../Models/deleteTable.php";
        foreach ($_SESSION["tablesToDelete"] as $table)
        {
            $result = deleteTable($nameDb,$table);
        }
    }
}

require_once "../Models/launchScript.php";
$sqlScript = fopen("../" . $pathToSqlScript, "r");
$sqlScriptContent = fread($sqlScript, filesize("../" . $pathToSqlScript));

foreach ($arrayTablesToCreate[0] as $tableToCreate)
{
    $search = array(ORIGINAL_PREFIX . "DB", ORIGINAL_PREFIX . $tableToCreate);
    $replace = array($nameDb, $_SESSION["prefix"] . $tableToCreate);
    $sqlScriptContent = str_replace($search, $replace, $sqlScriptContent);
}

launchScript($sqlScriptContent);
include "../Views/endOfInstall.php";