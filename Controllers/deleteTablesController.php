<?php
if(session_status() == 1)
{
    session_start();
}
require "../Models/getDataBase.php";

if (isset($_POST['submitDeleteTables']))
{
    require_once "../Models/deleteTable.php";

    if (isset($_SESSION['tablesToDelete']))
    {
        $_SESSION['tablesToDelete'] = $_POST['checkboxTable'];
    }

    include "../Views/addPrefix.php";
}
else
{   
    include "../Views/chooseDb.php";
}

