<?php
if(session_status() == 1)
{
    session_start();
}
require "../Models/mySql.inc.php";
require "../Models/getDataBase.php";
require_once "../Models/getRights.php";
require_once "../Models/dataBase.php";

if (isset($_POST['adressServer']))
{
        $serverAdress = filter_input(INPUT_POST, 'adressServer', FILTER_SANITIZE_STRING);
        $usernameServer = filter_input(INPUT_POST, 'usernameServer', FILTER_SANITIZE_STRING);
        $passwordServer = filter_input(INPUT_POST, 'passwordServer', FILTER_SANITIZE_STRING);

        $_SESSION["adressServer"] = $serverAdress;
        $_SESSION["usernameServer"] = $usernameServer;
        $_SESSION["passwordServer"] = $passwordServer;
        $db = dataBase::getInstance();
        $user = "'$usernameServer'@'localhost'";
        $_SESSION["rightsOnServer"] = getServerRights($user);
        $rightsOnDb = getDbRights($user);
        $flagRights = FALSE;

        if ($db == false )
        {
            $_SESSION["flag"] = true;
            include "../Views/connectDbServer.php";
        }
        else
        {
            foreach ($_SESSION["rightsOnServer"] as $right )
                    {
                        if  ($right['PRIVILEGE_TYPE'] == "CREATE")
                        {
                            $flagRights = TRUE;
                        }
                    }
            if ($flagRights != TRUE)
            {
                if ($rightsOnDb != FALSE)
                {
                    $flagRights = TRUE;
                }
                else
                {
                    $flagRights = FALSE;
                }
            }

            if ($flagRights == TRUE)
            {
                include "../Views/chooseDb.php";
            }
            else
            {
                $_SESSION["flag"] = true;
                include "../Views/connectDbServer.php";
            }
        }
}
else
{
    include "../Views/connectDbServer.php";

}
