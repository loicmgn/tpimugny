<?php
if(session_status() == 1)
{
    session_start();
}
if (isset($_POST['textPrefix']))
{

    if  ($_POST['radiosPrefix'] == "radioPrefix")
    {
        $prefix = filter_input(INPUT_POST, 'textPrefix', FILTER_SANITIZE_STRING);
    
        if  ($prefix != "")
        {
            $_SESSION['prefix'] = $prefix;
            include "../Views/chooseAccount.php";
        }
        else
        {
            $_SESSION["flag"] = true;
            include "../Views/addPrefix.php";
        }
    }
    else if ($_POST['radiosPrefix'] == "radioNoPrefix")
    {
        $_SESSION["prefix"] = ""; 
        include "../Views/chooseAccount.php";
    }

}
else
{
    include "../Views/chooseDb.php";
}