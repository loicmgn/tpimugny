<?php
if(session_status() == 1)
{
    session_start();
}
if (isset($_POST['submitChooseDb']))
{

    $radioValue = filter_input(INPUT_POST, 'radioChooseDb', FILTER_SANITIZE_STRING);

    if ($radioValue == "newDb")
    {
        $nameNewDb = filter_input(INPUT_POST, 'textNewDb', FILTER_SANITIZE_STRING);
        if ($nameNewDb != "")
        {
            require_once "../Models/createDataBase.php";
            include_once "../Views/addPrefix.php";
            unset($_SESSION["flag"]);
            $_SESSION["nameDb"] = $nameNewDb;
            $_SESSION["newDb"] = TRUE;         
        }
        else
        {
            $_SESSION["flag"] = true;
            include "../Views/chooseDb.php";
        }
    }
    else if ($radioValue == "existingDb")
    {
        require_once "../Models/getDataBase.php";
        $nameExistingDb = filter_input(INPUT_POST, 'selectExistingDb', FILTER_SANITIZE_STRING);
        $_SESSION["nameDb"] = $nameExistingDb;
        $_SESSION["newDb"] = FALSE;    
        $tablesFromDb = getDataBase($nameExistingDb);
        if($tablesFromDb != "")
        {
            include_once "../Views/deleteTables.php";
        }
        else
        {
            unset($_SESSION["flag"]);
            include "../Views/addPrefix.php";
        }
    }
}
else
{
    include "../Views/connectDbServer.php";
}
?>