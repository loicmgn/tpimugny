<?php
require_once "../config.php";
if (file_exists("../" . $pathToApplicationSqlConfig))
{
    require_once "../" . $pathToApplicationSqlConfig;
}
require_once "../Models/testConnection.php";
require_once "../Models/getDataBase.php";

if (isset($_POST["submitChooseInstall"]))
{
    $valueCombo = filter_input(INPUT_POST, 'radiosInstall', FILTER_SANITIZE_STRING);
    if ($valueCombo == "radioNewInstall")
    {
        file_put_contents("../" . $pathToApplicationSqlConfig,"");
        include "../Views/connectDbServer.php";
    }
    elseif ($valueCombo = "radioExistInstall")
    {
        include "../Views/existingInstall.html";
    }
}
else
{
    if (file_exists("../" . $pathToApplicationSqlConfig))
    {
        if (filesize("../" . $pathToApplicationSqlConfig) != 0)
        {
            $configSql = fopen("../" . $pathToApplicationSqlConfig, "r");
            $configSqlContent = fread($configSql, filesize("../" . $pathToApplicationSqlConfig));
            if  (strpos($configSqlContent, '$hostConfig') != FALSE && strpos($configSqlContent, '$usernameConfig') != FALSE && strpos($configSqlContent, '$passwordConfig') != FALSE)
            {
                if (testConnection($hostConfig, $usernameConfig, $passwordConfig))
                {
                    include "../Views/chooseInstall.php";
                }
            }
            else
            {
                include"../Views/connectDbServer.php";
            }        
        }
        else
        {
            include"../Views/connectDbServer.php";
        }
        
    }
    else
    {
        include"../Views/connectDbServer.php";
    }
    
}