<?php
if(session_status() == 1)
{
    session_start();
}
require "../Models/testConnection.php";
if (isset($_POST['btnSubmitAccount'])) {
    $radioChecked = $_POST['radioChooseAccount'];

    if ($radioChecked == "sameAccount") 
    {
        $_SESSION["usernameDb"] = $_SESSION["usernameServer"];
        $_SESSION["passwordDb"] = $_SESSION["passwordServer"];
        $_SESSION["flagEndOfInstall"] = TRUE ;
        include "../Controllers/endOfInstallController.php";
    } 
    else if ($radioChecked == "otherAccount") 
    {
        $usernameExistingAccount = filter_input(INPUT_POST, 'usernameExistingAccount', FILTER_SANITIZE_STRING);
        $passwordExistingAccount = filter_input(INPUT_POST, 'passwordExistingAccount', FILTER_SANITIZE_STRING);
        $host = $_SESSION["adressServer"];
        if (!testConnection($host,$usernameExistingAccount,$passwordExistingAccount)) {
            $_SESSION["flag1"] = true;
            include "../Views/chooseAccount.php";
        } 
        else {
            $_SESSION["usernameDb"] = $usernameExistingAccount;
            $_SESSION["passwordDb"] = $passwordExistingAccount;
            $_SESSION["flagEndOfInstall"] = TRUE ;
            header("../Controllers/endOfInstallController.php");
        }
    } 
    else if ($radioChecked == "newAccount") 
    {
        require_once "../Models/createAccount.php";
        $usernameNewAccount = filter_input(INPUT_POST, 'usernameNewAccount', FILTER_SANITIZE_STRING);
        $passwordNewAccount = filter_input(INPUT_POST, 'passwordNewAccount', FILTER_SANITIZE_STRING);

        if ($usernameNewAccount != "" && $passwordNewAccount != "") 
        {           
            $_SESSION["usernameDb"] = $usernameNewAccount;
            $_SESSION["passwordDb"] = $passwordNewAccount;
            createAccount($usernameNewAccount, $passwordNewAccount, $_SESSION["nameDb"]);
            $_SESSION["flagEndOfInstall"] = TRUE ;
            include "../Controllers/endOfInstallController.php";
        }
        else
        {
            $_SESSION["flag2"] = true;
            include "../Views/chooseAccount.php";
        }
    }
} 
else 
{
    include "../Views/addPrefix.php";
}
